#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <libve.h>

static struct s_battery bank[LIBVE_MAX_BATTERY_BANKS];
static pthread_t tid_bmv[LIBVE_MAX_BATTERY_BANKS];

static int parse_bmv(char *line)
{
	char *sp;
	char *key;
	char *val;

	key = strtok_r(line, "\t", &sp);
	val = strtok_r(NULL, "\t", &sp);

	if (strstr(key, "BMV")) {
	} else if (strstr(key, "V")) {
		bank[0].voltage = strtof(val, NULL)/1000.0;
	} else if (strstr(key, "TTG")) {
	} else if (strstr(key, "T")) {
		bank[0].temperature = strtof(val, NULL);
	} else if (strstr(key, "PID")) {
	} else if (strstr(key, "I")) {
		bank[0].current = strtof(val, NULL)/1000.0;
	} else if (strstr(key, "P")) {
		bank[0].power = strtof(val, NULL);
	} else if (strstr(key, "SOC")) {
		bank[0].SOC = strtof(val, NULL)/10.0;
	}
}

static void *libve_poll_if(void *arg)
{
	FILE *f;
	char *line;
	size_t n;

	f = fopen((const char*)arg, "r");

	while (1) {
		line = NULL;
		n = 0;
		getline(&line, &n, f);
		if (!n)
			continue;
		parse_bmv(line);
		free(line);
	}

	fclose(f);
}

int libve_poll(const char *interface)
{
	return pthread_create(&tid_bmv[0], NULL, &libve_poll_if, (void*)interface);
}

int libve_poll_stop(const int handle)
{
	return pthread_cancel(tid_bmv[0]);
}

int libve_get_battery(const int index, struct s_battery *bat)
{
	memcpy(bat, &bank[index], sizeof(struct s_battery));
	return 0;
}
