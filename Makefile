TOP = $(shell pwd)

export DESTDIR=
export PREFIX=/usr

SUBDIRS += src
SUBDIRS += examples

all: $(SUBDIRS)

.PHONY: subdirs $(SUBDIRS)

subdirs: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -C $@

test:
	LD_LIBRARY_PATH=$(TOP)/src $(TOP)/examples/libve-cli-monitor

install:
	$(MAKE) -C src install
	$(MAKE) -C examples install

clean:
	$(MAKE) -C src clean
	$(MAKE) -C examples clean
