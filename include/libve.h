#ifndef __LIBVE_H__
#define __LIBVE_H__

#define LIBVE_MAX_BATTERY_BANKS 4

struct s_battery {
	float voltage;
	float temperature;
	float current;
	float power;
	float SOC;
};

int libve_poll(const char *interface);
int libve_poll_stop(const int handle);
int libve_get_battery(const int index, struct s_battery *bat);

#endif
