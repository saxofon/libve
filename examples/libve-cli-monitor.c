#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <locale.h>
#include <pthread.h>
#include <sys/types.h>

#include <libve.h>

static void dump_ve_info_title(void)
{
	printf("Battery Temperature(°C) Voltage(V) Current(A) Power(W) SOC(%)\n");
}

static void dump_ve_info_data(struct s_battery *b)
{
	printf("%-7d %-15.1f %-10.2f %-10.3f %-8.2f %-10.2f\r",
		0, b->temperature, b->voltage, b->current, b->power, b->SOC);
}

int main(int argc, char **argv)
{
	int libve_handle;
	struct s_battery bat;

	setlocale(LC_ALL, "");

	libve_handle = libve_poll("/dev/ttyUSB0");

	dump_ve_info_title();
	while(1) {
		libve_get_battery(0, &bat);
		dump_ve_info_data(&bat);
		fflush(stdout);
		sleep(1);
	}

	libve_poll_stop(libve_handle);
}
